﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestTraining.Webservice.Controllers;
using UnitTestTraining.Webservice.Models;
using Xunit;

namespace UnitestTraining.UnitTest.Controller
{

    public class WeatherUnitTest
    {
        [Fact]
        public void Should_Be_Success (){

            //Arrange
            var controller = new WeatherForecastController();

            //Act
            var res = controller.GetWeathers();

            OkObjectResult result = res as OkObjectResult;
            OutputModel val = result.Value as OutputModel;
            

            //Assert
            Assert.NotNull(result);
            Assert.Equal(1, val.Code);
           

        }
    }
}
