﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestTraining.Webservice.Controllers;
using UnitTestTraining.Webservice.Models;
using Xunit;

namespace UnitestTraining.UnitTest.Controller
{
    public class UsersUnitTest
    {
        [Theory]
        [InlineData("flukefluke4069@gmail.com", "1234")]
        [InlineData("flukefluke4069@gmail.com", "1234ddddd")]
        [InlineData("flukefluke4069@gmail.com", "1234asdasdasd")]
        public void Login_Should_Be_Success(string Email, string Password)
        {
            var controller = new UsersController();

            var result = controller.Login(Email, Password);
            OkObjectResult ok = result as OkObjectResult;
            Assert.NotNull(ok);
            OutputModel output = ok.Value as OutputModel;
            Assert.IsType<OutputModel>(output);
            Assert.Equal(1, output.Code);
        }

        [Theory]
        [InlineData("flukefluke4069@gmail.com", "1234")]
        [InlineData("flukefluke4069@gmail.com", "1234ddddd")]
        [InlineData("flukefluke4069@gmail.com", "1234asdasdasd")]
        public void Login_Should_Be_Error(string Email, string Password)
        {
            var controller = new UsersController();

            var result = controller.Login(Email, Password);

            Action act = () => result.GetType();
            BadRequestObjectResult ok = result as BadRequestObjectResult;
            Assert.NotNull(ok);
            OutputModel output = ok.Value as OutputModel;
            Assert.IsType<OutputModel>(output);
            Assert.Equal(-1, output.Code);

            //ArgumentException exception = Assert.Throws<ArgumentException>(act);
            //Assert.Equal("Error",exception.Message);
        }

       
    }
}
