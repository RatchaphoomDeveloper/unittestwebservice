﻿using System;
using System.Collections.Generic;
using UnitTestTraining.Webservice.Models;
using System.Linq;
namespace UnitTestTraining.Webservice.Mockup
{
    public class UsersMockup
    {
        private static readonly List<Users> users = new List<Users>()
        {
            new Users()
            {
                Name = "Ratchaphoom",
                Lastname = "Boonnaka",
                Email = "flukefluke4069@gmail.com",
                Password = "1234",
                Age = 27,
            },
            new Users()
            {
                Name = "Tony",
                Lastname = "Stark",
                Email = "tonyironman@gmail.com",
                Password = "1234",
                Age = 50,
            },
            new Users()
            {
                Name = "Captain",
                Lastname = "America",
                Email = "captain007@gmail.com",
                Password = "1234",
                Age = 200,
            }
        };

        OutputModel output;

        private static UsersMockup _instance = null;
        public static UsersMockup Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UsersMockup();
                }
                return _instance;
            }
        }

        // Ins
        public OutputModel InsUser(Users data)
        {
            try
            {
                if (data == null || data.Email == null
                    || data.Password == null || data.Email == ""
                    || data.Password == "")
                {
                    return new OutputModel(-1, "Error", null);
                }
                else
                {
                    //List<Users> filterUser = users.Where(x => x.Email == data.Email).ToList();

                    foreach (var x in from x in users
                                      where x.Email.Equals(data.Email.Trim())
                                      select new
                                      {
                                          x.Name,
                                          x.Lastname,
                                          x.Email,
                                          x.Password,
                                          x.Age
                                      })
                    {
                        if (x.Email.Equals(data.Email))
                        {
                            return new OutputModel(-1, "Error", null);
                        }

                    }

                    users.Add(data);

                }

                return new OutputModel(1, "Success", null);
            }
            catch (Exception ex)
            {

                throw new Exception("InsUser " + ex.Message);
            }
        }

        // Get
        public OutputModel GetUsers()
        {
            try
            {
                if (users.Count == 0)
                {
                    return new OutputModel(-1, "Error", null);
                }

                return new OutputModel(1, "Success", users);
            }
            catch (Exception ex)
            {

                throw new Exception("GetUsers " + ex.Message);
            }
        }

        public OutputModel Login(string Email, string Password)
        {
            try
            {
                List<Users> loginData = null;
                if (Email == null
                    || Password == null || Email == ""
                    || Password == "")
                {
                    return new OutputModel(-1, "Error", null);
                }
                else
                {
                    loginData = users.Where(x => x.Email.Equals(Email) && x.Password.Equals(Password)).ToList();
                }

                return loginData.Count == 0 ? new OutputModel(-1, "Error", null) : new OutputModel(1,"Success",loginData);
            }
            catch (Exception ex)
            {

                throw new Exception("Login " + ex.Message);
            }
        }

        public OutputModel GetUserOne(string Email)
        {
            try
            {

                List<Users> loginData = null;
                if (Email == null|| Email == "")
                {
                    return new OutputModel(-1, "Error", null);
                }
                else
                {
                    loginData = users.Where(x => x.Email.Equals(Email) ).ToList();
                }

                return loginData.Count == 0 ? new OutputModel(1, "Error", null) : new OutputModel(1, "Success", loginData);
            }
            catch (Exception ex)
            {

                throw new Exception("GetUserOne " + ex.Message);
            }
        }


    }
}
