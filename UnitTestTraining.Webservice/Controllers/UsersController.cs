﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UnitTestTraining.Webservice.Mockup;
using UnitTestTraining.Webservice.Models;

namespace UnitTestTraining.Webservice.Controllers
{
   
    public class UsersController : ControllerBase
    {

        public UsersController() { }

        [HttpPost]
        [Route("api/Login")]
        public IActionResult Login(string Email , string Password) {
            try
            {
                var output = new OutputModel();
                output = UsersMockup.Ins.Login(Email, Password);
                return output.Code == 1 ? Ok(output) : BadRequest(output);
            }
            catch (System.Exception ex)
            {

                return BadRequest(new { Message = "Login  " + ex.Message });
            }
        }
    }
}
