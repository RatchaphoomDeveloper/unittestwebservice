﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnitTestTraining.Webservice.Models;

namespace UnitTestTraining.Webservice.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };


        public WeatherForecastController() { }


        [HttpGet]
        [Route("GetWeathers")]
        public IActionResult GetWeathers()
        {
            try
            {
                var rng = new Random();
                var output = new OutputModel()
                {
                    Code = 1,
                    Message = "Success",
                    Data = Enumerable.Range(1, 5).Select(index => new WeatherForecast
                    {
                        Date = DateTime.Now.AddDays(index),
                        TemperatureC = rng.Next(-20, 55),
                        Summary = Summaries[rng.Next(Summaries.Length)]
                    })
                .ToArray()
                };

                return Ok(output);
                
              
            }
            catch (Exception ex)
            {

                return BadRequest(new { Message = "GetWeathers "+ex.Message });

            }
           
        }
    }
}
