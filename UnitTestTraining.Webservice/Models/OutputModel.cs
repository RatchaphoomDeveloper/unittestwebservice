﻿namespace UnitTestTraining.Webservice.Models
{
    public class OutputModel
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }

        public OutputModel() { }

        public OutputModel(int code ,string message , object data)
        {
            this.Code = code;
            this.Message = message;
            this.Data = data;
        }
    }
}
