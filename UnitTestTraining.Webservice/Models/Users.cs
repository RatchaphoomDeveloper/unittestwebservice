﻿namespace UnitTestTraining.Webservice.Models
{
    public class Users
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
    }
}
